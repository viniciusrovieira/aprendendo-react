import Button from "./components/Button";
import React from "react";

const App = () => {

  const [dados, setDados] = React.useState(null);
  const [loading, setLoading] = React.useState(null)
  return (
    <>
      <div className="botoes">
        <Button setDados={setDados} setLoading={setLoading} produto="notebook" />
        <Button setDados={setDados} setLoading={setLoading} produto="smartphone" />
        <Button setDados={setDados} setLoading={setLoading} produto="tablet" />
      </div>
      <div className="produto">
          {loading && <p>Carregamento...</p> }
          {!loading && dados && 
          
          <ul>
              <li><span>Nome: </span>{dados.nome}</li>
              <li><span>Preço: </span>{dados.preco}</li>
              <li><span>Descrição: </span>{dados.descricao}</li>
              <li><img style={{width:"200px",height:"400px", objectFit:'contain'}} src={dados.fotos[0].src} alt="" /></li>
            </ul>
            
            }
        
      </div>
    </>
  );
};

export default App;
