import React from 'react';
import Header from './components/Header';
import Produtos from './components/Produtos';
import Home from './components/Home';

const App = () => {
  const { pathname } = window.location;

  let Component;

  if (pathname === '/produtos') {
    Component = Produtos;
  } else {
    Component = Home;
  }

  return (
    <>
      <Header />
      <Component />
    </>
  );
};

export default App;
