import React from 'react';

const Header = () => {
  return (
    <div>
      <header>
        <nav>
          <ul>
            <li>
              <a href="/">Home</a>
            </li>
            <li>
              <a href="/produtos">Produtos</a>
            </li>
          </ul>
        </nav>
      </header>
    </div>
  );
};

export default Header;
