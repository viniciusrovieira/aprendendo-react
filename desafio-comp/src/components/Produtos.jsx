import React from 'react';

const Produtos = () => {
  const produtos = [
    { nome: 'Notebook', propriedades: ['16gb ram', '512gb'] },
    { nome: 'Smartphone', propriedades: ['2gb ram', '128gb'] },
  ];

  return (
    <div>
      <h2>Produtos</h2>
      {produtos.map(({ nome, propriedades }) => (
        <section>
          <h2>{nome}</h2>
          <ul>
            {propriedades.map((produto) => (
              <li>{produto}</li>
            ))}
          </ul>
        </section>
      ))}
    </div>
  );
};

export default Produtos;
