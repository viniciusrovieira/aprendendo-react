import React from "react";

const Button = ({ produto, setDados,setLoading }) => {
  async function handleFetch() {
    setLoading(true);
    const response = await fetch(`https://ranekapi.origamid.dev/json/api/produto/${produto}`);
    const json = await response.json();
    setDados(json)
    setLoading(false);
  }
  return (
    <div>
      <button onClick={handleFetch}>{produto}</button>
    </div>
  );
};

export default Button;
