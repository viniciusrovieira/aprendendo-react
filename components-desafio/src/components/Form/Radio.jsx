import React from "react";

const Radio = ({ options,pergunta,id ,onChange,value,active}) => {
  if(active === false) return null
  return (
    <>
    <fieldset style={{marginBottom:'5rem',padding:'15px'}}>
          <legend style={{fontWeight:'bold'}}>{pergunta}</legend>
      {options.map((option)=> (
        <label key={option} style={{fontFamily:'monospace'}}>
            <input type="radio" checked={value === option} value={option} onChange={onChange} id={id}/>
            {option}
        </label>
      ))}
        </fieldset>

    </>
  );
};

export default Radio;
