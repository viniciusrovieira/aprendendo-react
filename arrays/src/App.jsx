const luana = {
  cliente: 'Luana',
  idade: 27,
  compras: [
    { nome: 'Notebook', preco: 'R$ 2500' },
    { nome: 'Geladeira', preco: 'R$ 3000' },
    { nome: 'Smartphone', preco: 'R$ 1500' },
  ],
  ativa: true,
};

const mario = {
  cliente: 'Mario',
  idade: 31,
  compras: [
    { nome: 'Notebook', preco: 'R$ 2500' },
    { nome: 'Geladeira', preco: 'R$ 3000' },
    { nome: 'Smartphone', preco: 'R$ 1500' },
    { nome: 'Guitarra', preco: 'R$ 3500' },
  ],
  ativa: false,
};

const App = () => {
  const filmes = [
    'O Poderoso Chefão',
    'Cidadão Kane',
    'O Senhor dos Anéis: A Sociedade do Anel',
    'O Labirinto do Fauno',
    'Interestelar',
    'O Grande Lebowski',
    'A Origem',
    'Clube da Luta',
  ];

  const livros = [
    { nome: 'A Game of Thrones', ano: 1996 },
    { nome: 'A Clash of Kings', ano: 1998 },
    { nome: 'A Storm of Swords', ano: 2000 },
  ];
  const produtos = [
    {
      id: 1,
      nome: 'Smartphone',
      preco: 'R$ 2000',
      cores: ['#29d8d5', '#252a34', '#fc3766'],
    },
    {
      id: 2,
      nome: 'Notebook',
      preco: 'R$ 3000',
      cores: ['#ffd045', '#d4394b', '#f37c59'],
    },
    {
      id: 3,
      nome: 'Tablet',
      preco: 'R$ 1500',
      cores: ['#365069', '#47c1c8', '#f95786'],
    },
  ];
  return (
    <>
      <h1>Array</h1>
      <ul>
        {filmes.map((filme) => (
          <li key={filme}>{filme}</li>
        ))}
      </ul>
      <hr />
      <h1>Array de Objetos</h1>
      <ul>
        {livros
          .filter(({ ano }) => ano >= 1998)
          .map(({ nome, ano }) => (
            <li key={nome}>
              {nome} - {ano}
            </li>
          ))}
      </ul>

      <section>
        <hr />
        <h1>Array com filtros</h1>
        {produtos
          .filter((produto) => Number(produto.preco.replace('R$ ', '')) > 1500)
          .map((produto) => (
            <div key={produto.id}>
              <h1>{produto.nome}</h1>
              <p>Preço: {produto.preco}</p>
              <ul>
                {produto.cores.map((cor) => (
                  <li key={cor} style={{ backgroundColor: cor, color: '#fff' }}>
                    {cor}
                  </li>
                ))}
              </ul>
            </div>
          ))}
      </section>
    </>
  );
};

export default App;
