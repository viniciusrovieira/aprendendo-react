import React from 'react';
import {BrowserRouter, Routes, Route} from 'react-router-dom'
import Home from './components/Home'
import Header from './components/Header';
import NotFound from './components/NotFound';
import Sobre from './components/Sobre';

const App = () => {


  return (
    <>
      <BrowserRouter>
      <Header/>
        <Routes>
            <Route path='/' element={<Home/>}/>
            <Route path='sobre' element={<Sobre/>}/>
            <Route path='*' element={<NotFound/>}/>
        </Routes>
      </BrowserRouter>
    </>
  );
};

export default App;
