const luana = {
  cliente: 'Luana',
  idade: 27,
  compras: [
    { nome: 'Notebook', preco: 'R$ 2500' },
    { nome: 'Geladeira', preco: 'R$ 3000' },
    { nome: 'Smartphone', preco: 'R$ 1500' },
  ],
  ativa: true,
};

const mario = {
  cliente: 'Mario',
  idade: 31,
  compras: [
    { nome: 'Notebook', preco: 'R$ 2500' },
    { nome: 'Geladeira', preco: 'R$ 3000' },
    { nome: 'Smartphone', preco: 'R$ 1500' },
    { nome: 'Guitarra', preco: 'R$ 3500' },
  ],
  ativa: false,
};

const App = () => {
  const dados = mario;
  const total = dados.compras
    .map((item) => Number(item.preco.replace('R$ ', '')))
    .reduce((a, b) => a + b);
  console.log(total);
  return (
    <div>
      <h3>Nome: {dados.cliente}</h3>
      <h3>Idade:{dados.idade}</h3>
      <h3>
        Status:
        <span className={dados.ativa ? 'Ativo' : 'Inativo'}>
          {dados.ativa ? 'Ativa' : 'Inativo'}
        </span>
      </h3>
      <h3>Total:R${total}</h3>
      {total > 10000 && <span> 'Você está gastando muito!'</span>}
    </div>
  );
};

export default App;
