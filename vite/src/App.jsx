import React from 'react';

const App = () => {
  return (
    <>
      <h1>React</h1>
      <label htmlFor="nome">Nome: </label>
      <input type="text" name="nome" id="nome" />
    </>
  );
};

export default App;
