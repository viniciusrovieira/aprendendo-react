import React from 'react';
import Header from './components/Header';
import Footer from './components/Footer';
import Form from './components/Form';

const App = () => {
  return (
    <>
      <Header />
      <h2>Esse é meu primeiro formulário</h2>
      <Form />
      <Footer />
    </>
  );
};

export default App;
