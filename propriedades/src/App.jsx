import React from 'react';

const Titulo = ({ cor, texto, children, ...props }) => {
  return (
    <h1
      style={{
        backgroundColor: cor,
        padding: '15px',
        color: '#fff',
        borderRadius: '15px',
      }}
      {...props}
    >
      {texto},{children}
    </h1>
  );
};

const App = () => {
  return (
    <section>
      <Titulo cor="red" texto="Meu titulo 1">
        Isso é um children
      </Titulo>
      <Titulo cor="green" texto="Meu titulo 2" title="Teste" />
      <Titulo cor="blue" texto="Meu titulo 3" />
    </section>
  );
};

export default App;
