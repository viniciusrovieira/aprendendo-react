import React from "react";
import styles from "./Contato.module.css";
import foto from "../img/contato.jpg";
import Head from './Head'

const Contato = () => {
  return (
    <section className={`${styles.contato} animeLeft`}>
      <Head title="Ranek | Contato" description="Entre em contato" />
      <img src={foto} alt="Máquina de escrever" />
      <div className="">
        <h1>Entre em contato.</h1>
        <ul className={styles.dados}>
          <li>vinicius_cs16@hotmail.com</li>
          <li>99999-99999</li>
          <li>Rua Ali Perto, 955</li>
        </ul>
      </div>
    </section>
  );
};

export default Contato;
