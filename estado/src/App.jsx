import React from 'react';
import ButtonModal from './components/ButtonModal';
import Modal from './components/Modal';

const App = () => {
  const [modal, setModal] = React.useState(false);

  return (
    <>
      <div>{modal ? 'Modal Aberto' : 'Modal Fechado'}</div>
      <Modal modal={modal} setModal={setModal} />
      <ButtonModal modal={modal} setModal={setModal} />
    </>
  );
};

export default App;
