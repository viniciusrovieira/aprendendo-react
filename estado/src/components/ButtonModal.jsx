import React from 'react';

const ButtonModal = ({ modal, setModal }) => {
  function handleClick() {
    setModal(true);
  }
  if (!modal)
    return (
      <div>
        <button onClick={handleClick}>Abrir</button>
      </div>
    );
  else null;
};

export default ButtonModal;
