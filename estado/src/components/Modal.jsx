import React from 'react';

const Modal = ({ modal, setModal }) => {
  function handleClick() {
    setModal(false);
  }
  if (modal)
    return (
      <div>
        <h1>Este é meu modal!</h1>
        <button onClick={handleClick}>Fechar</button>
      </div>
    );
  else null;
};

export default Modal;
