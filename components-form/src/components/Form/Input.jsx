import React from 'react'

const Input = ({id,label,setValue,value,...props}) => {
  return (
    <div>
        <label htmlFor={id}>{label}</label>
        <input name={id} {...props} onChange={({target})=> setValue(target.value)}/>
        <p>{value}</p>
    </div>
  )
}

export default Input
