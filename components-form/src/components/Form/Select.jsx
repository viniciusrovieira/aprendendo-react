import React from 'react'

const Select = ({options,value,setValue}) => {
  return (
    <div>   
        <select value={value} onChange={({target})=> setValue(target.value)}>
            <option disabled value='' >Selecione</option>
            {options.map((option)=> <option key={option} value={option}>{option}</option>) }
        </select>
      
    </div>
  )
}

export default Select
