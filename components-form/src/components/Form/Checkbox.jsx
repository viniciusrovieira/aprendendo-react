import React from "react";

const Checkbox = ({ value, setValue, options, ...props }) => {
  function handleChange({ target }) {
    if (target.checked) {
      setValue([...value, target.value]);
    } else {
      setValue(
        value.filter((itemValue) => itemValue !== target.value)
      );
    }
  }
  return (
    <div>
      {options.map((option) => (
        <label htmlFor={option} key={option}>
          <input
            type="checkbox"
            onChange={handleChange}
            id={option}
            value={option}
            checked={value.includes(option)}
            {...props}
          />
          {option}
        </label>
      ))}
    </div>
  );
};

export default Checkbox;
