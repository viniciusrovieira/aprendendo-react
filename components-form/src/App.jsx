import React from "react";
import Input from "./components/Form/Input";
import Select from "./components/Form/Select";
import Radio from "./components/Form/Radio";
import Checkbox from "./components/Form/Checkbox";

const App = () => {
  const [nome, setNome] = React.useState("");
  const [email, setEmail] = React.useState("");
  const [produto, setProduto] = React.useState("");
  const [cor, setCor] = React.useState('');
  const [programa, setPrograma] = React.useState([])

  return (
    <>
      <form action="">
        <Checkbox value={programa} setValue={setPrograma} options={['PHP', 'NodeJS', 'React']}/>
      <Radio
        options={['azul', 'verde', 'amarelo']}
        value={cor}
        setValue={setCor}
      />
        <Select
          value={produto}
          setValue={setProduto}
          options={["Smartphone", "Tablet"]}
          />

        <Input
          type="text"
          id="nome"
          label="Nome"
          value={nome}
          setValue={setNome}
        />

        <Input
          type="email"
          id="email"
          label="E-mail"
          value={email}
          setValue={setEmail}
        />
      </form>
    </>
  );
};

export default App;
