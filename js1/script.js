//Objeto ---------------------------

// const menu = {
//   seletor: '.principal',
// };
// console.log(menu.seletor.toUpperCase());

// Arrow Function ---------------------------
// function upperName(name) {
//   return name.toUpperCase();
// }
// console.log(upperName('Vinicius'));

// Arrow function reduzido--------------------------
// const upperName = (name) => name.toUpperCase();

//Event ---------------

// function handleMouse({clientX,clientY}) {
//   console.log(clientX,clientY);
// }
// document.addEventListener('click', handleMouse);

//Desinstruturação ---------------

// const useQuadrado = [
//   4,
//   function (lado) {
//     return 4 * lado;
//   },
// ];
// const [lados, perimetros] = useQuadrado;
// console.log(lados);
// console.log(perimetros(4));

// import { areaQuadrado } from './quadrado.js';
// console.log(areaQuadrado(5));

// fetch('https://ranekapi.origamid.dev/json/api/produto')
//   .then((response) => response.json())
//   .then((json) => {
//     console.log(json);
//   });

async function fetchProduto(url) {
  const response = await fetch(url);
  const json = await response.json();
  return json;
}
const produtos = fetchProduto('https://ranekapi.origamid.dev/json/api/produto');
console.log(produtos);
