import React, { useContext } from "react";
import { GlobalContext } from "./GlobalContext";

const Produto = () => {
  const {dados, limparDados} = useContext(GlobalContext);
    if(dados == null) return null 
  return (
    <div>
        <h2>Produtos:</h2>
        <ul>
            {dados.map((prd)=> <li key={prd.id}>{prd.nome} - R$ {prd.preco}</li>)}
        </ul>
    </div>
  );
};

export default Produto;
