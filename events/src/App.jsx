const App = () => {
  function handleClick({ target, pageX, pageY }) {
    console.log(target, pageX, pageY);
  }
  return (
    <>
      <button onClick={handleClick} onMouseMove={handleClick}>
        Clique
      </button>
    </>
  );
};

export default App;
